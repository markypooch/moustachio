FROM rust:1.39-stretch

COPY . /
WORKDIR /

RUN cargo build

ENTRYPOINT [ "./target/debug/mustachio.exe" ]