extern crate ws;

use std::rc::Rc;
use std::cell::Cell;

struct Handler {
    out:               ws::Sender,
    count:             Rc<Cell<usize>>,
    playerDescription: String,
    x : f32,
    y : f32,
    id: usize,
}

impl ws::Handler for Handler {
    fn on_open(&mut self, _:ws::Handshake) -> std::result::Result<(), ws::Error> {

        self.count.set(self.count.get() + 1);
        self.id = self.count.get();

        self.playerDescription = format!("
        {{
            \"initial\":true,
            \"player\":
            {{
                \"id\":{id}, 
                \"position\": [0.0, 0.0, -8.0]
            }}
        }}", id=self.count.get());
        self.out.broadcast(self.playerDescription.clone());
        
        return Ok(());
    }

    
    fn on_message(&mut self, msg: ws::Message) -> Result<(), ws::Error> {
        let raw_text = String::from(msg.into_text()?);
        let positions = raw_text.split(",");

        // depending on the keycodes sent, increase position
        for position in positions {
            match position {
                "87" => self.y += 0.045,
                "83" => self.y -= 0.045,
                "65" => self.x -= 0.045,
                "68" => self.x += 0.045,
                _ => (),
            }
        }

        //TODO: validate the proposed move (doesn't collide through solid object) from client
        // is valid

        self.playerDescription = format!("
        {{\"player\":
            {{
                \"connClose\":false,
                \"id\":{id}, 
                \"position\": [{x}, {y}, -8.0]
            }}
        }}", id=self.id, x=self.x, y=self.y);
        self.out.broadcast(self.playerDescription.clone());
        return Ok(());
    }

    fn on_close(&mut self, code: ws::CloseCode, reason: &str) {
        self.playerDescription = format!("
        {{\"player\":
            {{
                \"connClose\":true,
                \"id\":{id}, 
                \"position\": [{x}, {y}, -8.0]
            }}
        }}", id=self.id, x=self.x, y=self.y);
        self.out.broadcast(self.playerDescription.clone()); 
    }
}

fn main() {
    let count   = Rc::new(Cell::new(0));
    let address = String::from("127.0.0.1:5000");
    if let Err(error) = ws::listen(address, |out| {
        Handler {
            out: out,
            playerDescription: String::from(""),
            count: count.clone(),
            x: 0.0,
            y: 0.0,
            id: 0,
        }
    }) {
        println!("Failed to create WebSocket due to {:?}", error);
    }
}
