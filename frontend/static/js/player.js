class Player {
    constructor(gl, shaderManager, textureManager, modelManager) {
        this.gl = gl
        this.shaderManager = shaderManager
        this.textureManager = textureManager
        this.texture        = null
        this.shader         = null
        this.model          = null
        this.position       = []
    }

    initialize() {
        this.texture = this.textureManager.loadTexture("fish", "https://fishyio.s3.amazonaws.com/fishy.png");
        this.shader  = this.shaderManager.loadShader({'vertexPosition':'aVertexPosition', 'texCoord':'aTexCoord'}, 
                        {'projectionMatrix': 'uProjectionMatrix', 'viewMatrix':'uViewMatrix', 'modelViewMatrix':'uModelMatrix', 'uSampler':'uSampler'}, 
                        "sprite", 
                        spriteVSShader, 
                        spritePSShader);
        this.model   = modelManager.loadSprite();
    }

    update(playerPosition) {
        this.position = playerPosition;
        this.model.setPosition(this.position, 0.0, [0, 0, 0])
    }

    render() {
        this.gl.cullFace(this.gl.FRONT);
        this.gl.useProgram(this.shader.programInfo.program);
        this.texture.bindTexture(this.shader.programInfo);
        this.model.render(camera, this.shader.programInfo);
    }
}