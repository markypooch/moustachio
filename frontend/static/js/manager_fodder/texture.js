class Texture {
    constructor(gl, texture) {
        this.gl = gl;
        this.texture = texture;
    }

    bindTexture(shaderProgramInfo) {
        
        this.gl.activeTexture(this.gl.TEXTURE0);
        this.gl.bindTexture(this.gl.TEXTURE_2D, this.texture);
        this.gl.uniform1i(shaderProgramInfo.uniformLocations.uSampler, 0);
    }
}