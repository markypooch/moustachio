class Model {
    constructor(gl, vertex_count) {
        this.gl = gl;
        this.modelViewMatrix = mat4.create();
        this.vertex_count    = vertex_count;
        this.vertexBuffer    = null;
        this.normalBuffer    = null;
        this.uvBuffer        = null;
        this.material        = {'color':[], 'transparency':0};
        this.drawMode        = this.gl.TRIANGLES;

        this.translationMatrix = mat4.create();
        this.rotationMatrix    = mat4.create();
    }

    setPosition(position, rotation, axis) {
        const translationMatrix = mat4.create();
        const rotationMatrix    = mat4.create();

        mat4.translate(translationMatrix,
            translationMatrix,
            position);
        mat4.fromRotation(rotationMatrix, rotation, axis);
        mat4.multiply(this.modelViewMatrix, translationMatrix, rotationMatrix);
        
    }

    render(camera, programInfo) {

        
        numComponents = 3;
        const type = this.gl.FLOAT;
        const normalize = false;
        const stride = 0;
        const offset = 0;

        {
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.vertexBuffer);
            this.gl.vertexAttribPointer(
                programInfo.attribLocations.vertexPosition,
                numComponents,
                type,
                normalize,
                stride,
                offset);

            this.gl.enableVertexAttribArray(
                programInfo.attribLocations.vertexPosition);
        }

        if (this.normalBuffer != null)
        {
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.normalBuffer);
            this.gl.vertexAttribPointer(
                programInfo.attribLocations.normalPosition,
                numComponents,
                type,
                normalize,
                stride,
                offset);
            this.gl.enableVertexAttribArray(
                programInfo.attribLocations.normalPosition);
        }
        if (this.uvBuffer != null) {
            var numComponents = 2;

            this.gl.bindBuffer(this.gl.ARRAY_BUFFER, this.uvBuffer);
            this.gl.vertexAttribPointer(
                programInfo.attribLocations.texCoord,
                numComponents,
                type,
                normalize,
                stride,
                offset);
            this.gl.enableVertexAttribArray(
                programInfo.attribLocations.texCoord);
        }
        
        
      
        this.gl.uniformMatrix4fv(
            programInfo.uniformLocations.projectionMatrix,
            false,
            camera.projectionMatrix);
        this.gl.uniformMatrix4fv(
            programInfo.uniformLocations.modelViewMatrix,
            false,
            this.modelViewMatrix);
        this.gl.uniformMatrix4fv(
            programInfo.uniformLocations.viewMatrix,
            false,
            camera.viewMatrix);
    
        {
          const offset = 0;
          this.gl.drawArrays(this.drawMode, offset, this.vertex_count/3);
        }
    }
}