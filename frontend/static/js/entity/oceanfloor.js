class Oceanfloor {
    constructor(gl, camera, modelmanager, shadermanager) {
        this.gl           = gl;
        this.camera       = camera;
        this.seabed       = modelmanager.loadModel("seabed_Plane", seabed);
        this.outline      = modelmanager.loadModel("outline", outline);

        this.shader = shaderManager.loadShader({'vertexPosition':'aVertexPosition', 'normalPosition':'aNormalPosition'}, 
            {'projectionMatrix': 'uProjectionMatrix', 'viewMatrix':'uViewMatrix', 'modelViewMatrix':'uModelMatrix', 'color':'uColor'}, 
            "passthrough", 
            vsShader, 
            psShader);
    }

    update(dt) {
        this.seabed.setPosition([0.0, -3.0, -15.0], 3.12, [0, 1, 0]);
        this.outline.setPosition([0.0, -3.0, -15.0], 3.12, [0, 1, 0]);
    }

    render() {

        this.gl.useProgram(this.shader.programInfo.program);
        this.gl.uniform3fv(this.shader.programInfo.uniformLocations.color, this.seabed.material['color']);
        this.seabed.render(camera, this.shader.programInfo);

        this.gl.cullFace(this.gl.FRONT);
        this.gl.uniform3fv(this.shader.programInfo.uniformLocations.color, this.outline.material['color']);
        this.outline.render(camera, this.shader.programInfo);
        this.gl.cullFace(this.gl.BACK);
    }
}