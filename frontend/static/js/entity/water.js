class Water {
    constructor(gl, camera, modelmanager, shadermanager) {
        this.gl          = gl;
        this.camera      = camera;
        this.water       = modelmanager.loadModel("off", water);
        this.water.material['transparency'] = 0.2;

        this.watershader = shadermanager.loadShader({'vertexPosition':'aVertexPosition', 'normalPosition':'aNormalPosition'}, 
            {'projectionMatrix': 'uProjectionMatrix', 'viewMatrix':'uViewMatrix', 'modelViewMatrix':'uModelMatrix', 'color':'uColor', 'transparency':'uTransparency', 't':'uT'}, 
            "water", 
            waterVSShader, 
            waterPSShader);

        this.time  = 0.0;
    }

    update(dt) {
        this.time += 0.45;
        //if (this.time >= 1.0) this.time = 0.0;
        
        this.water.setPosition([0.0, 4.5, -15.0], 0, [0, 0, 0])

    }

    render() {
        this.gl.cullFace(renderer.gl.FRONT);

        this.gl.useProgram(this.watershader.programInfo.program);
        this.gl.uniform1f(this.watershader.programInfo.uniformLocations.t, this.time);
        this.gl.uniform1f(this.watershader.programInfo.uniformLocations.transparency, this.water.material['transparency']);
        this.gl.uniform3fv(this.watershader.programInfo.uniformLocations.color, this.water.material['color']);

        this.water.render(camera, this.watershader.programInfo, false);

        this.gl.cullFace(renderer.gl.BACK);
    }
}