function keyboardCallback(event) {
    var key = event.key || event.keyCode;
    keys[event.keyCode] = event.type;
}

// Giddy Glad Globals
position = [0.0, 0.0, 0.0];
id       = -1;
players  = {};
keys     = {};

window.addEventListener("keydown", keyboardCallback);
window.addEventListener("keyup", keyboardCallback);
window.onload = function() {

    // instantiate managers
    renderer       = new Renderer();
    camera         = new Camera(renderer.gl, [0.0, 0.0, 0.0]);
    modelManager   = new ModelManager(renderer.gl);
    shaderManager  = new ShaderManager(renderer.gl);
    textureManager = new TextureManager(renderer.gl);

    // instantiate the oceanfloor, water, and the local player
    water      = new Water(renderer.gl, camera, modelManager, shaderManager);
    oceanfloor = new Oceanfloor(renderer.gl, camera, modelManager, shaderManager);
    player     = new Player(renderer.gl, shaderManager, textureManager, modelManager);
    player.initialize();

    // enable culling, and attempt to establish a connection with our ws server
    renderer.gl.enable(renderer.gl.CULL_FACE);
    let socket = new WebSocket("ws://127.0.0.1:5000/ws");

    socket.onopen = function(event) {};

    socket.onmessage = function(event) {
        processOnMessage(event, 
                         id, 
                         position, 
                         players, 
                         renderer.gl, 
                         shaderManager, 
                         textureManager, 
                         modelManager);
    };

    socket.onclose = function(event) {
        if (event.wasClean) {
           alert(event.data);
        }
    };

    socket.onerror = function(error) {};
    function mainLoop() {
        var socket_message = ""
        for (key in keys) {
            if (keys[key] == 'keydown') {
                if (key == '87') {
                    socket_message += "87,"
                }
                if (key == '83') {
                    socket_message += "83,"
                }
                if (key == '65') {
                    socket_message += "65,"
                }
                if (key == '68') {
                    socket_message += "68,"
                }
            }
        }

        renderer.resize();
        renderer.clear();

        camera.update([-position[0], 0.0, 0.0]);

        water.update();
        water.render();

        oceanfloor.update();
        oceanfloor.render();

        player.update(position);
        player.render();

        for (remotePlayer in players) {
            players[remotePlayer].update(players[remotePlayer].position);
            players[remotePlayer].render();
        }

        window.requestAnimationFrame(mainLoop);
        if (socket_message != "")
            socket.send(socket_message);
    };
    window.requestAnimationFrame(mainLoop);
}
