class Renderer {
    constructor() {
        this.canvas = document.getElementById("canvas");

        this.gl = canvas.getContext("webgl");
        if (this.gl == null) {
            alert("Shit");
        }

        this.gl.enable(this.gl.BLEND);
        this.gl.blendFunc(this.gl.SRC_ALPHA, this.gl.ONE_MINUS_SRC_ALPHA);
    }

    initShaderProgram(vsSource, fsSource) {

        var vertexShader = this.compileShader(this.gl.VERTEX_SHADER, vsSource);
        var fragmentShader = this.compileShader(this.gl.FRAGMENT_SHADER, fsSource);
    
        var shaderProgram = this.gl.createProgram();
        this.gl.attachShader(shaderProgram, vertexShader);
        this.gl.attachShader(shaderProgram, fragmentShader);
        this.gl.linkProgram(shaderProgram);
    
        if (!this.gl.getProgramParameter(shaderProgram, this.gl.LINK_STATUS)) {
            alert('Unable to initialize the shader program: ' + this.gl.getProgramInfoLog(this.shaderProgram));
            return null;
        }
        
        /*var programInfo = {program: shaderProgram}
        var attribPositions = {};
        for (var i = 0; i < attribLocations.length; i++) {
            attribPositions[attribLocations[i].split(":")[0]] = this.gl.getAttribLocation(shaderProgram, attribLocations[i].split(":")[1]);
        }

        var uniformPositions = {};
        for (var i = 0; i < uniformLocations.length; i++) {
            uniformPositions[uniformLocations[i].split(":")[0]] = this.gl.getUniformLocation(shaderProgram, uniformLocations[i].split(":")[1]);
        }
        programInfo['attribLocations'] = attribPositions;
        programInfo['uniformLocations'] = uniformPositions;*/

        return shaderProgram;
    }
    
    compileShader(type, source) {
        this.shader = this.gl.createShader(type);
    
        this.gl.shaderSource(this.shader, source);
        this.gl.compileShader(this.shader);
    
        // See if it compiled successfully
        if (!this.gl.getShaderParameter(this.shader, this.gl.COMPILE_STATUS)) {
            alert('An error occurred compiling the shaders: ' + this.gl.getShaderInfoLog(this.shader));
            this.gl.deleteShader(this.shader);
            return null;
        }
    
        return this.shader;
    }

    clear() {
        this.gl.clearColor(0.2, 0.4, 0.6, 1.0);  // Clear to black, fully opaque
        this.gl.clearDepth(1.0);                 // Clear everything
        this.gl.enable(this.gl.DEPTH_TEST);           // Enable depth testing
        this.gl.depthFunc(this.gl.LEQUAL);            // Near things obscure far things
      
        // Clear the canvas before we start drawing on it.
        this.gl.clear(this.gl.COLOR_BUFFER_BIT | this.gl.DEPTH_BUFFER_BIT);
    }

    resize() {
        
        // Lookup the size the browser is displaying the canvas.
        var displayWidth  = this.canvas.clientWidth;
        var displayHeight = this.canvas.clientHeight;
        
        // Check if the canvas is not the same size.
        if (canvas.width  != displayWidth ||
            canvas.height != displayHeight) {
        
            // Make the canvas the same size
            canvas.width  = displayWidth;
            canvas.height = displayHeight;
        }
        this.gl.viewport(0, 0, this.gl.canvas.width, this.gl.canvas.height);
    }
}