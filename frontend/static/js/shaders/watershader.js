waterVSShader = `attribute vec4 aVertexPosition;
attribute vec4 aNormalPosition;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;
uniform float uT;
varying highp vec3 vLighting;

void main() {
  
  float h = 0.3 * sin(0.9 * (aVertexPosition.x - 0.1 * uT));

  vec4 n = aNormalPosition;
  n.y -= h;
  highp vec4 transformedNormal = uModelMatrix * normalize(n);

  vec4 p = aVertexPosition;
  p.y += h;

  gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * p;
  
  vec3 ambientLight = vec3(0.3, 0.3, 0.3);
  vec3 directionalLightColor = vec3(1, 1, 1);
  vec3 directionalVector = normalize(vec3(0.85, -0.2, 0.0));
  
  highp float directional = max(dot(transformedNormal.xyz, directionalVector), 0.0);

  if (directional < 0.4) directional = 0.4;
  else if (directional > 0.4 && directional < 0.8) directional = 0.6;
  else if (directional > 0.8) directional = 0.8;

  vLighting = ambientLight + (directionalLightColor * directional);


}
`;

waterPSShader = `varying highp vec3 vLighting;
uniform highp vec3 uColor;
uniform highp float uTransparency;

void main() {
  

  gl_FragColor = vec4(vec4(uColor,1.0) * vec4(vLighting,1.0));
  gl_FragColor.w = uTransparency;
  //gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
}
`;