spriteVSShader = `attribute vec4 aVertexPosition;
attribute vec2 aTexCoord;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;

varying highp vec2 vTextureCoord;

void main() {
  
  gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * aVertexPosition;
  vTextureCoord = aTexCoord;
}
`;

spritePSShader = `varying highp vec2 vTextureCoord;
uniform sampler2D uSampler;

void main() {
    gl_FragColor = texture2D(uSampler, vTextureCoord);
    //gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);
}
`;