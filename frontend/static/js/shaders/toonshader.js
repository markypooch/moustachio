vsShader = `attribute vec4 aVertexPosition;
attribute vec4 aNormalPosition;

uniform mat4 uModelMatrix;
uniform mat4 uViewMatrix;
uniform mat4 uProjectionMatrix;

varying highp vec3 vLighting;

void main() {
  
  gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * aVertexPosition;
  
  vec3 ambientLight = vec3(0.5, 0.5, 0.5);
  vec3 directionalLightColor = vec3(1, 1, 1);
  vec3 directionalVector = normalize(vec3(0.85, -0.2, 0.0));
  
  highp vec4 transformedNormal = uModelMatrix * aNormalPosition;
  highp float directional = max(dot(transformedNormal.xyz, directionalVector), 0.0);

  if (directional < 0.4) directional = 0.4;
  else if (directional > 0.4 && directional < 0.8) directional = 0.6;
  else if (directional > 0.8) directional = 0.8;

  vLighting = ambientLight + (directionalLightColor * directional);
}
`;

psShader = `varying highp vec3 vLighting;
uniform highp vec3 uColor;

void main() {
  

  gl_FragColor = vec4(vec4(uColor,1.0) * vec4(vLighting,1.0));
  //gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);
}
`;