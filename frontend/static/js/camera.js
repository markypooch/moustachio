class Camera {
    constructor(gl, initial_position) {
        this.gl = gl;
        this.viewMatrix = mat4.create();
        mat4.translate(this.viewMatrix,
                       this.viewMatrix,
                       initial_position);

        this.projectionMatrix = mat4.create();
        
        const fieldOfView = 45 * Math.PI / 180;   // in radians
        const aspect = this.gl.canvas.clientWidth / this.gl.canvas.clientHeight;
        const zNear = 0.1;
        const zFar = 200.0;
        
        mat4.perspective(this.projectionMatrix,
            fieldOfView,
            aspect,
            zNear,
            zFar);

        this.translationMatrix = mat4.create();
        this.rotationMatrix    = mat4.create();
    }

    update(position) {
        const translationMatrix = mat4.create();
        const rotationMatrix    = mat4.create();

        mat4.translate(translationMatrix,
            translationMatrix,
            position);
        mat4.fromRotation(rotationMatrix, 0.0, [0.0, 0.0, 0.0]);
        mat4.multiply(this.viewMatrix, translationMatrix, rotationMatrix);
    }
}