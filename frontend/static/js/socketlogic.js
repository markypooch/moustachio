function processOnMessage(event, localPlayerID, position, players, gl, shaderManager, textureManager, modelManager) {

    jsonPlayerPosition = JSON.parse(event.data);
    if (localPlayerID == -1 && jsonPlayerPosition.initial) {
        localPlayerID      = jsonPlayerPosition.player.id;
        position           = jsonPlayerPosition.player.position;
    } else {
        var player_id      = jsonPlayerPosition.player.id;

        if (localPlayerID != jsonPlayerPosition.player.id) {
            if (!jsonPlayerPosition.player.connClose) {
                if (player_id in players) {
                    players[player_id].position = jsonPlayerPosition.player.position;
                }
                else {
                    players[player_id] = new Player(gl, shaderManager, textureManager, modelManager);
                    players[player_id].initialize();
                }
            }
            else {
                delete players[player_id];
            }
        }
        else {
            position = jsonPlayerPosition.player.position;
        }
    }
}