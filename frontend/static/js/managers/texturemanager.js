class TextureManager extends Manager {
    constructor(gl) {
        super(gl);
        this.gl = gl;
    }

    isPowerOf2(value) {
        return (value & (value - 1)) == 0;
    }
    
    loadTexture(textureName, url) {
        if (!(textureName in this.resource_dict)) {
            this.resource_dict[textureName] = new Texture(this.gl, this.gl.createTexture());
            this.gl.bindTexture(this.gl.TEXTURE_2D, this.resource_dict[textureName].texture);

            // put a single pixel in the texture so we can
            // use it immediately. When the image has finished downloading
            // we'll update the texture with the contents of the image.
            const level = 0;
            const internalFormat = this.gl.RGBA;
            const width = 1;
            const height = 1;
            const border = 0;
            const srcFormat = this.gl.RGBA;
            const srcType = this.gl.UNSIGNED_BYTE;
            const pixel = new Uint8Array([0, 0, 255, 255]);  // opaque blue
            this.gl.texImage2D(this.gl.TEXTURE_2D, level, internalFormat,
                            width, height, border, srcFormat, srcType,
                            pixel);

            const image = new Image();
            var gl = this.gl;
            var resource_dict = this.resource_dict;
            image.crossOrigin = "";
            var isPowerOf2 = this.isPowerOf2;
            image.onload = function() {
                gl.bindTexture(gl.TEXTURE_2D, resource_dict[textureName].texture);
                gl.texImage2D(gl.TEXTURE_2D, level, internalFormat,
                            srcFormat, srcType, image);

                if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
                    // Yes, it's a power of 2. Generate mips.
                    gl.generateMipmap(gl.TEXTURE_2D);
                }
            };
            image.src = url;
        }

        return this.resource_dict[textureName];
    }
}