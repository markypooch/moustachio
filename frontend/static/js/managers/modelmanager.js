class ModelManager extends Manager {
    constructor(gl) {
        super(gl);
        this.gl      = gl;
        this.backend = window.API_HOST;
    }

    loadSprite() {

        if (!("quad" in this.resource_dict)) {
        
            var vertices = [
                -1.0,  1.0, 0.0,
                1.0,  1.0, 0.0,
                -1.0, -1.0, 0.0,
                1.0, -1.0, 0.0,   
            ]
            
            var uvs = [
                0.0,  0.0,
                1.0,  0.0,
                0.0,  1.0,
                1.0,  1.0,
            ]
            this.resource_dict['quad'] = new Model(this.gl, vertices.length);

            this.build_buffer(this.resource_dict['quad'], vertices, null, uvs);
            this.resource_dict['quad'].drawMode = this.gl.TRIANGLE_STRIP;
            return this.resource_dict['quad'];
        }
        return this.resource_dict['quad'];
    }

    loadModel(model_name, model_data) {
        if (!(model_name in this.resource_dict)) {
        
            this.resource_dict[model_name] = new Model(this.gl, model_data[model_name].v.length);

            this.build_buffer(this.resource_dict[model_name], model_data[model_name].v, model_data[model_name].vn, null);
            this.resource_dict[model_name].material['color'] = model_data[model_name].color;

            return this.resource_dict[model_name];
        }
        return this.resource_dict[model_name];
    }

    build_buffer(model, vertices, normals, uvs) {
        console.log(vertices);
        model.vertexBuffer = this.gl.createBuffer();

        // Select the positionBuffer as the one to apply buffer
        // operations to from here out.
        this.gl.bindBuffer(this.gl.ARRAY_BUFFER,  model.vertexBuffer);

        this.gl.bufferData(this.gl.ARRAY_BUFFER,
            new Float32Array(vertices),
            this.gl.STATIC_DRAW);
        
        if (normals != undefined && normals != null) {

            model.normalBuffer = this.gl.createBuffer();

            // Select the positionBuffer as the one to apply buffer
            // operations to from here out.
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER,  model.normalBuffer);

            this.gl.bufferData(this.gl.ARRAY_BUFFER,
                new Float32Array(normals),
                this.gl.STATIC_DRAW);
        }
        if (uvs != undefined && uvs != null) {
            model.uvBuffer = this.gl.createBuffer();

            // Select the positionBuffer as the one to apply buffer
            // operations to from here out.
            this.gl.bindBuffer(this.gl.ARRAY_BUFFER,  model.uvBuffer);

            this.gl.bufferData(this.gl.ARRAY_BUFFER,
                new Float32Array(uvs),
                this.gl.STATIC_DRAW);
        }
    }
}