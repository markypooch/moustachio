class Manager {
    constructor(gl) {
        this.resource_dict = {};
        this.gl = gl;
    }

    retrieveResource(backend, model_name) {
        if (!(model_name in this.resource_dict)) {

            request = new XMLHttpRequest();
            request.onreadystatechange = function() {
                if (request.status == 200) {
                    this.resource_dict[model_name] = request.response;
                    return true;
                }
            }

            request.setRequestHeader("Content-Type", "application/json");
            request.responseType = 'blob';
            request.open("GET", `${backend}/assets/${model_name}`);
            request.send();
        }
        else {
            return false;
        }
    }
}