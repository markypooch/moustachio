class ShaderManager extends Manager {
    constructor(gl) {
        super(gl);
        this.gl = gl;
    }

    loadShader(attribLocations, uniformLocations, shadername, vsSource, fsSource) {
        if (!(shadername in this.resource_dict)) {

            var vertexShader = this.compileShader(this.gl.VERTEX_SHADER, vsSource);
            var fragmentShader = this.compileShader(this.gl.FRAGMENT_SHADER, fsSource);
        
            var shaderProgram = this.gl.createProgram();
            this.gl.attachShader(shaderProgram, vertexShader);
            this.gl.attachShader(shaderProgram, fragmentShader);
            this.gl.linkProgram(shaderProgram);
        
            if (!this.gl.getProgramParameter(shaderProgram, this.gl.LINK_STATUS)) {
                alert('Unable to initialize the shader program: ' + this.gl.getProgramInfoLog(this.shaderProgram));
                return null;
            }
            
            var programInfo = {program: shaderProgram}
            var attribPositions = {};
            for (var key in attribLocations) {
                attribPositions[key] = this.gl.getAttribLocation(shaderProgram, attribLocations[key]);
            }

            var uniformPositions = {};
            for (var key in uniformLocations) {
                uniformPositions[key] = this.gl.getUniformLocation(shaderProgram, uniformLocations[key]);
            }
            programInfo['attribLocations'] = attribPositions;
            programInfo['uniformLocations'] = uniformPositions;

            var shader = new Shader(this.gl, programInfo);
            this.resource_dict[shadername] = shader;
        }

        return this.resource_dict[shadername];
    }
    
    compileShader(type, source) {
        this.shader = this.gl.createShader(type);
    
        this.gl.shaderSource(this.shader, source);
        this.gl.compileShader(this.shader);
    
        // See if it compiled successfully
        if (!this.gl.getShaderParameter(this.shader, this.gl.COMPILE_STATUS)) {
            alert('An error occurred compiling the shaders: ' + this.gl.getShaderInfoLog(this.shader));
            this.gl.deleteShader(this.shader);
            return null;
        }
    
        return this.shader;
    }
}